# ChemScraper Containerization

This document details how to install, configure and debug the first live version of ChemScraper delivered to the Alpha Synthesis project.

## Installation

Tested on Ubuntu versions 22.04 (NVIDIA GeForce RTX 3060) and 20.04 (NVIDIA GeForce RTX 2080ti). 
- Docker version 24.0.2 
- NVIDIA Container Toolkit CLI version 1.13.1


### Installation Steps

- Follow install instructions: [Docker Engine](https://docs.docker.com/engine/install/ubuntu/) using the `apt repository` method

- Follow install instructions: [Docker Nvidia](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker) using the instructions for `docker` (not CDI or podman)

  - Optionally add current user to docker group where `$USER` is replaced by the real linux user name. If this step is omitted `sudo` will always be required


  ```bash
  sudo groupadd docker
  sudo usermod -aG docker $USER
  ```



- Reboot machine to let changes take affect

- Run the following commands to see what versions of Docker and Nvidia Docker have been installed

  ```bash
  docker --version
  nvidia-ctk --version
  ```

- run the following commands
	```bash
	git clone https://gitlab.com/dprl/dprl-alphasynthesis.git
	cd dprl-alphasynthesis
	docker compose up
	```

## Running The Services

After running  `docker compose up` the docker images will start to download. This could take a couple minutes, but this operation only needs to be run once. 

Once the services are up and running go to `http://localhost:8000/docs` There should be a swagger api documentation page. Test the `extractPdf` endpoint with the attached PDF `or100.09.tables.pdf`. This will take a couple seconds to run. The output should look like what is present below. This our PRO table representation, to learn more about it check out the [repository](https://gitlab.com/dprl/protables)
```tsv
D	1	/inputs/tmp_inpdfs/test.pdf
P	2	1000	1000
FR	1	827	1064	999	1193
SMI	1	CC\C(=C(/*)C1=CC=CC=C1)C1=CC=CC=C1	827	1064	999	1193
FR	2	830	824	1020	906
SMI	2	CC(CC1=CC=CC=C1)C(Br)=C	830	824	1020	906
FR	3	1613	1063	1794	1195
SMI	3	CCCC\C(=C(/CC)C1=CC=CC=C1)C1=CC=CC=C1	1613	1063	1794	1195
...
```



## Debugging

The `ChemScraper` service depends on two other services `yolo` and `sscraper`. These both have their own swagger API documentation pages which can be found at `http://localhost:8005/docs` for `yolo` and `http://localhost:7002/swagger-ui` for `sscraper`. If something does go wrong hitting these endpoints can help determine what part of the system is failing